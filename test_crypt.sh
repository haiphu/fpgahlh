#!/bin/bash
cd ~/Documents/thesis/numato/xdma
time=10;
test=2;
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo ""
            exit 0
        ;;
        -i)
            sudo insmod xdma.ko
            sudo insmod huy_crypto.ko
            exit 0
        ;;
        -m)
            make clean
            make
            exit 0
        ;;
        -r)
            sudo rmmod pre_test.ko | sudo rmmod testcrypto.ko
            exit 0
        ;;
        -v)
            shift
            sudo dmesg -C
            sudo insmod test_data.ko test_choice=3 fir=$1 end=$2
            sudo rmmod test_data.ko
            dmesg
            exit 0
        ;;
        -d)
            shift
            time=$1
            shift
        ;;
        -t)
            shift
            test=$1
            shift
        ;;
            *)
            break
        ;;
    esac
done
sudo dmesg -C
sudo insmod pre_test.ko time=$time test_choice=$test | sudo insmod testcrypto.ko time=$time test_choice=$test
dmesg
sudo rmmod pre_test.ko | sudo rmmod testcrypto.ko